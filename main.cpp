#include <iostream> // klasiski visām c++ programmām, standarta ievade / izvade
#include <stdlib.h> // klasiski visām c++ programmām, standarta metodes
#include <array> // ērtākai uzdevumu veikšanai, pievienota masīvu bibliotēka

using namespace std;// lai atsaucoties uz standarta metodēm, nav jāraksta std::cout..., tad šeit pasaka, ka tiks izmantota attiecīgā 'vārdu telpa' (īsti nezinu kā pareizi tulko 'namespace' :) )

// metodes pirmajam variantam
// ērtības, lasāmības dēļ -- atsevišķa metode, kas izkāpina veselu skaitli
int getSquare(int x) {
  return x * x;
}

int maxSqRoot(int square) {
  int target = 1, root;

  // nulle ir īpaš gadījums, kam nav vajadzīga tālāka apstrāde, rezultāts zināms
  if (square == 0)
    return 0;

  // ja nu skaitlis nav nulle, tad vajag pārbaudīt
  for (root = 1; target <= square; root++) { // katru veselu skaitli, kas ir robežās no 1 līdz dotajam kvadrātam
    target = getSquare(root); // kāpina kvadrātā

    if (target >= square) { // ja iegūtais kvadrāts ir vienāds vai lielāks par doto kvadrātu,
      if ((target - square) < (square - getSquare(root - 1))) { // pārbauda vai iegūtā un dotā kvadrāta starpība ir mazāka par iepriekšējā solī iegūto,
        break; // ja tā ir taisnība, ir atrasts lielākais veselais skaitlis, kas nepārsnieddz dotā skaitļa kvadrātsakni, ciklu var pārtraukt
      } else {
        --root;
        break; // ja tomēr iegūtā un dotā kvadrāta starpība ir lielāka, tad meklētais skaitlis tika atrasts iepriekējā solī, ciklu var pārtraukt
      }
    }
  }
  return root; // kad cikls beidzies, atgriež atrasto skaitli
}
/// pirmā varianta beigas



//metodes otrajam variantam
// ērtības, lasāmības dēļ -- atsevišķa metode, kas kāpina skaitli kubā
int getCube(int x) {
  return x * x * x;
}

int minCubeRoot(int cube) {
  int target = 1, root;

  // nulle ir īpaš gadījums, kam nav vajadzīga tālāka apstrāde, rezultāts zināms
  if (cube == 0)
    return 0;

  // ja nu skaitlis nav nulle, tad vajag pārbaudīt
  for (root = 1; target <= cube; root++) { // katru veselu skaitli, kas ir robežās no 1 līdz dotajam kubam
    target = getCube(root); // ceļ kubā..

    if (target >= cube) // tiklīdz iegūtais kubs ir vienāds vai lielāks ar doto,
      break; // tad ir atrasts mazākais veselais skaitlis, kas nav mazāks par dotā kuba sakni, ciklu var pārtraukt
  }

  return root; // un atgriezt rezultātu
}
// otrā varianta beigas




int main(int argc, char *argv[]) { // klasiski, c++ programma no komandrindas saņem divus parametrus -- argumentu skaitu (argc) un norādi uz argumentu masīvu (*argv)
  // mājsaimniecības daļa, kas vienāda abiem variantiem
  int arrSize = argc-1; // tā kā pirmais arguments ir programmas nosaukums, tad iekšējais masīvs būs par vienu mazāks
  int q[arrSize]; // izveido veselu skaitļu masīvu ar nepieciešamo izmēru

  // argumenti tiek iedoti kā teksts, tos vajag konvertēt no string uz int, to dara atoi() metode;
  // PIEZĪME: tā kā uzdevuma nostādnē ir minēts, ka ir doti N nenegatīvi skaitļi, tad ir pieņemts, ka ievades dati būs korekti,
  // netiek veikta argumentu pārbaude; ja kaut kur parādīsies teksts, rezultāts ir neprognozējams
  for (int i = 1; i < argc; i++) { // tā kā pirmais arguments ir programmas nosaukums, jāapstrādā sākot no 'viens'
    q[i-1] = atoi(argv[i]); // taču, masīva elementu skaitīšana sākas no nulles, tas jāņem vērā -> i-1
  }

  cout << "1.variants -- atrast lielāko veselo skaitli, kas nepārsniedz kvadrātsakni" << endl;
  for (int i = 0; i < arrSize; i++) {
    cout << q[i] << " -> " << maxSqRoot(q[i]) << "; ";
  }
  cout << endl;

  cout << "2.variants risinājums -- atrast mazāko veselo skaitli, kas nav mazāks par kubsakni" << endl;
  for (int i = 0; i < arrSize; i++) {
    cout << q[i] << " -> " << minCubeRoot(q[i]) << "; ";
  }

  cout << endl;

  return(0);
}
